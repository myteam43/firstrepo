package com.company;

public interface Shape {
    default int calcArea(){
        return 10;
    }
}
